
package com.panin.mavenwebspring.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorporateID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorporateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdditionalID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Branch" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AutoPickUp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TokenSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corporateID",
    "corporateName",
    "customerID",
    "customerName",
    "additionalID",
    "channel",
    "branch",
    "autoPickUp",
    "tokenSerialNumber"
})
@XmlRootElement(name = "registerCorporateUser")
public class RegisterCorporateUser {

    @XmlElement(name = "CorporateID", required = true)
    protected String corporateID;
    @XmlElement(name = "CorporateName", required = true)
    protected String corporateName;
    @XmlElement(name = "CustomerID", required = true)
    protected String customerID;
    @XmlElement(name = "CustomerName", required = true)
    protected String customerName;
    @XmlElement(name = "AdditionalID", required = true)
    protected String additionalID;
    @XmlElement(name = "Channel", required = true)
    protected String channel;
    @XmlElement(name = "Branch", required = true)
    protected String branch;
    @XmlElement(name = "AutoPickUp", required = true)
    protected String autoPickUp;
    @XmlElement(name = "TokenSerialNumber", required = true)
    protected String tokenSerialNumber;

    /**
     * Gets the value of the corporateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateID() {
        return corporateID;
    }

    /**
     * Sets the value of the corporateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateID(String value) {
        this.corporateID = value;
    }

    /**
     * Gets the value of the corporateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateName() {
        return corporateName;
    }

    /**
     * Sets the value of the corporateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateName(String value) {
        this.corporateName = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerID(String value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the additionalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalID() {
        return additionalID;
    }

    /**
     * Sets the value of the additionalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalID(String value) {
        this.additionalID = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Gets the value of the branch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranch() {
        return branch;
    }

    /**
     * Sets the value of the branch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranch(String value) {
        this.branch = value;
    }

    /**
     * Gets the value of the autoPickUp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoPickUp() {
        return autoPickUp;
    }

    /**
     * Sets the value of the autoPickUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoPickUp(String value) {
        this.autoPickUp = value;
    }

    /**
     * Gets the value of the tokenSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenSerialNumber() {
        return tokenSerialNumber;
    }

    /**
     * Sets the value of the tokenSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenSerialNumber(String value) {
        this.tokenSerialNumber = value;
    }

}
