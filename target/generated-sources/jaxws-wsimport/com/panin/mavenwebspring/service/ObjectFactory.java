
package com.panin.mavenwebspring.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.panin.mavenwebspring.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.panin.mavenwebspring.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTokenMoreInfo }
     * 
     */
    public GetTokenMoreInfo createGetTokenMoreInfo() {
        return new GetTokenMoreInfo();
    }

    /**
     * Create an instance of {@link VerifyROResponse }
     * 
     */
    public VerifyROResponse createVerifyROResponse() {
        return new VerifyROResponse();
    }

    /**
     * Create an instance of {@link ActivateCustomerChannelROResponse }
     * 
     */
    public ActivateCustomerChannelROResponse createActivateCustomerChannelROResponse() {
        return new ActivateCustomerChannelROResponse();
    }

    /**
     * Create an instance of {@link RegisterCorporateUser }
     * 
     */
    public RegisterCorporateUser createRegisterCorporateUser() {
        return new RegisterCorporateUser();
    }

    /**
     * Create an instance of {@link VerifyRO }
     * 
     */
    public VerifyRO createVerifyRO() {
        return new VerifyRO();
    }

    /**
     * Create an instance of {@link RegisterCorporateUserResponse }
     * 
     */
    public RegisterCorporateUserResponse createRegisterCorporateUserResponse() {
        return new RegisterCorporateUserResponse();
    }

    /**
     * Create an instance of {@link VerifyCR }
     * 
     */
    public VerifyCR createVerifyCR() {
        return new VerifyCR();
    }

    /**
     * Create an instance of {@link GetTokenMoreInfoResponse }
     * 
     */
    public GetTokenMoreInfoResponse createGetTokenMoreInfoResponse() {
        return new GetTokenMoreInfoResponse();
    }

    /**
     * Create an instance of {@link AddCustomerRegistrationResponse }
     * 
     */
    public AddCustomerRegistrationResponse createAddCustomerRegistrationResponse() {
        return new AddCustomerRegistrationResponse();
    }

    /**
     * Create an instance of {@link ActivateCustomerChannelRO }
     * 
     */
    public ActivateCustomerChannelRO createActivateCustomerChannelRO() {
        return new ActivateCustomerChannelRO();
    }

    /**
     * Create an instance of {@link VerifyCRResponse }
     * 
     */
    public VerifyCRResponse createVerifyCRResponse() {
        return new VerifyCRResponse();
    }

    /**
     * Create an instance of {@link AddCustomerRegistration }
     * 
     */
    public AddCustomerRegistration createAddCustomerRegistration() {
        return new AddCustomerRegistration();
    }

}
