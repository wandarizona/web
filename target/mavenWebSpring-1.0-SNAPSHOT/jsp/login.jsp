<%-- 
    Document   : login
    Created on : Sep 20, 2021, 12:48:10 PM
    Author     : User
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="resources/csslogin/fonts/icomoon/style.css">

    <link rel="stylesheet" type="text/css" href="resources/csslogin/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="resources/csslogin/css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="resources/csslogin/css/style.css">

    <title>Login</title>
  </head>
  <body>
  

  <div class="half">
    <div class="bg order-1 order-md-2" style="background-image: url('resources/csslogin/images/bg_3.jpg');"></div>
    <div class="contents order-2 order-md-1">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-6">
            <div class="form-block">
              <div class="text-center mb-5">
              <h3>Login <strong>Pajak</strong></h3>
               <p class="mb-4">Please input your login ID in capital letter.
Use your token code on your token. To avoid abuse, the code will appear randomly.</p> 
              </div>
              <form:form id="loginForm" modelAttribute="login" action="submit" method="post">
                <div class="form-group first">
                  <label for="username" path="username">Corporate ID</label>
                  <form:input type="text" class="form-control" placeholder="Corporate ID" id="CORPORATE_ID" path="CORPORATE_ID" onkeyup="this.value = this.value.toUpperCase();"></form:input>
                </div>
                  <div class="form-group first">
                  <label for="username">User ID</label>
                  <form:input type="text" class="form-control" placeholder="User ID" id="USERNAME" path="USERNAME" onkeyup="this.value = this.value.toUpperCase();"></form:input>
                </div>
                <div class="form-group last mb-3">
                  <label for="password">Token Code (APPLl 1)</label>
                <form:input type="password" class="form-control" placeholder="Token Code" id="TOKEN" path="TOKEN"></form:input>
                </div>
                
                <input type="submit" value="Log In" class="btn btn-block btn-primary">

              </form:form>
            </div>
              <table align="center">
                <tr>
                    <td style="font-style: italic; color: red;">${message}</td>
                </tr>
            </table>
          </div>
        </div>
      </div>
    </div>

    
  </div>
    
    

    <script src="resources/csslogin/js/jquery-3.3.1.min.js"></script>
    <script src="resources/csslogin/js/popper.min.js"></script>
    <script src="resources/csslogin/js/bootstrap.min.js"></script>
    <script src="resources/csslogin/js/main.js"></script>
  </body>
</html>
