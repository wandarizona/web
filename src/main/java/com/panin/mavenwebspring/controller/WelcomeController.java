/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.controller;

import com.panin.mavenwebspring.model.Login;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author User
 */
@Controller
public class WelcomeController {
   
  @RequestMapping(value = "/welcome", method = RequestMethod.GET)
  public ModelAndView showWelcome(HttpServletRequest request, HttpServletResponse response) {
    ModelAndView mav = new ModelAndView("login");
     mav.addObject("login", new Login());
            mav.addObject("message", "Mohon input corporateid username dan token dengan benar!!");
    HttpSession session=request.getSession(false);
    if (session != null){
        String username=(String)session.getAttribute("username");
        if (username != null){
            mav = new ModelAndView("welcome");
            mav.addObject("firstname", username);
            } else {
            mav = new ModelAndView("login");
           
        }
    }
    return mav;
  }  
}
