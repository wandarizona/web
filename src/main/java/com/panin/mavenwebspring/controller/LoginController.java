/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.controller;

import com.panin.mavenwebspring.model.Form;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.panin.mavenwebspring.model.Login;
import com.panin.mavenwebspring.model.User;
import com.panin.mavenwebspring.model.map;
import com.panin.mavenwebspring.service.MapService;
import com.panin.mavenwebspring.service.UserService;
import com.panin.mavenwebspring.service.VModuleService;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

@Controller
public class LoginController {
  private static final Logger logger = Logger.getLogger(LoginController.class.getName());
  private static boolean bResultWS = false;
  private static List<String> respVelis;
  private static String sURL;
  @Autowired
  UserService userService;
  @Autowired  
  MapService mapService;

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
    ModelAndView mav = new ModelAndView("login");
    mav.addObject("login", new Login());

    return mav;
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
  @ModelAttribute("login") Login login) {
    ModelAndView mav = null;

     logger.info("====================== POST submit START ==============================");
    
    try
    {
        String propFileName = "data.properties";
          InputStream inputStream;
          inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
          Properties prop = new Properties();
          if (inputStream!=null) {
            prop.load(inputStream);
          }
          sURL = prop.getProperty("velis.url");
          logger.info(" URL : " + sURL);
          String velisSimulator = prop.getProperty("velis.sim");
          logger.debug(" velisSimulator = " + velisSimulator);
        User user = userService.validateUser(login);
        if (null != user) {
            if (velisSimulator.equals("1"))
              {
                  bResultWS = true;
              }else  verifyToken(user,login);
            if (bResultWS)
            {
            HttpSession session=request.getSession();
            session.setAttribute("username", user.getUSERNAME());
            mav = new ModelAndView("form");
            mav.addObject("firstname", user.getUSER_FULLNAME());
            mav.addObject("countryList", "");
            mav.addObject("form", new Form());
            List<map> countryList;
            countryList = mapService.getAllMAP();
            mav.addObject("mapList",countryList);
            } else {
            mav = new ModelAndView("login");            
            mav.addObject("message", respVelis.get(1));
            }        
        }else {
            mav = new ModelAndView("login");
            mav.addObject("message", "Username or Password is wrong!!");
        }     
    }catch (Exception ex) {
            logger.error(ex.toString());
    }
        
        logger.info("====================== POST submit END ==============================");
        logger.info("                            -                                      ");
    return mav;
  }

  
  private List<String> verifyToken(User user,Login login)
  {      
      List<String> sResult;
      try
      {         
          
          URL url = new URL(sURL);
          logger.info(" Call WS Velis : ");
          com.panin.mavenwebspring.service.VModuleService wsData = new  com.panin.mavenwebspring.service.VModuleService(url);
          com.panin.mavenwebspring.service.VModule wsDataSoap= wsData.getVModule(); 
          logger.debug(" Call WS Velis : 1 ");
              javax.xml.ws.AsyncHandler<com.panin.mavenwebspring.service.VerifyROResponse> asyncHandler =
              new javax.xml.ws.AsyncHandler<com.panin.mavenwebspring.service.VerifyROResponse>() {
                    public void handleResponse(javax.xml.ws.Response<com.panin.mavenwebspring.service.VerifyROResponse> response) {
                          try { 
                                List<String> sResponse= response.get().getVerifyROReturn();
                                logger.info("Response Code : " + sResponse.get(0));
                                logger.info("Response Desc : " + sResponse.get(1));
                                //System.out.println(result);
                                respVelis =  sResponse;
                                if (sResponse.get(0).equals("0000")) 
                                {
                                    bResultWS = true;
                                    logger.info("Response Velis SUCCESS");
                                }else logger.info("Response Velis FAIL");
                                
                          } catch(Exception ex) {
                                // TODO handle exception
                                logger.info("Call WS Velis FAIL");
                                logger.error(ex.toString());
                          }
                    }
              };
              
              logger.debug(user.getCORPORATE_ID() + " | " + user.getUSERNAME() + " | " + user.getUSER_ID()+ " | " + login.getTOKEN());
              logger.debug(" Call WS Velis : 2 ");
              Random random = new Random();              
              java.util.concurrent.Future<? extends java.lang.Object> result = wsDataSoap.verifyROAsync(Integer.toString(random.nextInt(10000))
                      , login.getCORPORATE_ID(), user.getUSER_ID(), "IBB", login.getTOKEN(), asyncHandler); 
              logger.debug(" Call WS Velis : 3 ");
              while(!result.isDone()) {
                    // do something
                    Thread.sleep(100);
              }        
      }catch (Exception ex) {
          logger.error(ex.toString());
      }
      return respVelis;
      
  }
}