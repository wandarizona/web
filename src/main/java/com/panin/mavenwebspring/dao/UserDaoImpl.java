/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.panin.mavenwebspring.model.Login;
import com.panin.mavenwebspring.model.User;
import java.io.Console;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class UserDaoImpl implements UserDao {

    private static final Logger logger = Logger.getLogger(UserDao.class.getName());
  @Autowired
  DataSource datasource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  public User validateUser(Login login) {
    String sql = "select * from s_users where USERNAME='" + login.getUSERNAME()+ "' and CORPORATE_ID='" + login.getCORPORATE_ID()
        + "'";
    List<User> users = jdbcTemplate.query(sql, new UserMapper());
    System.out.println("validateUser");
      System.out.println(users.size());
    return users.size() > 0 ? users.get(0) : null;
  }
  
  public User getUser(Login login) {
    String sql = "select * from s_users where username='" + login.getUSERNAME() + "' and corporate_id='" + login.getCORPORATE_ID()
        + "'";
    logger.error(sql);
    List<User> users =  jdbcTemplate.query(sql, new UserMapper());
    logger.error(users.size());
    return users.size() > 0 ? users.get(0) : null;
  }

public List<User> getAllUser() {
      List<User> users = new ArrayList<User>();
      try
      {
    String sql = "select * from s_users ";
    logger.error(sql);
      users =  jdbcTemplate.query(sql, new UserMapper());
      }catch (Exception ex)
      {
          logger.error(ex.toString());
      }
    return users;
  }

}

class UserMapper implements RowMapper<User> {

  public User mapRow(ResultSet rs, int arg1) throws SQLException {
    User user = new User();

    user.setUSERNAME(rs.getString("USERNAME"));
    user.setCORPORATE_ID(rs.getString("CORPORATE_ID"));
    user.setUSER_FULLNAME(rs.getString("USER_FULLNAME"));
    user.setUSER_ID(rs.getString("USER_ID"));
    user.setINACTIVE_DATE(rs.getString("INACTIVE_DATE"));

    return user;
  }
}
