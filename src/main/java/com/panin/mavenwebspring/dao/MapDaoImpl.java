/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.dao;

import com.panin.mavenwebspring.model.User;
import com.panin.mavenwebspring.model.map;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author User
 */
public class MapDaoImpl implements MapDao{
    private static final Logger logger = Logger.getLogger(MapDaoImpl.class.getName());
    @Autowired
  DataSource datasource;

  @Autowired
  JdbcTemplate jdbcTemplate;
  
  public map getMAP(){
      String sql = "select distinct map_code, map_desc from s_tax_code order by map_desc";
     
    List<map> maps =  jdbcTemplate.query(sql, new MapMapper());

    return maps.size() > 0 ? maps.get(0) : null;
  }  
  
  public List<map> getAllMAP(){
      String sql = "select distinct map_code, map_desc from s_tax_code order by map_desc";
       logger.info("getAllMAP SQL = " + sql);
    List<map> maps =  jdbcTemplate.query(sql, new MapMapper());
     logger.info("Size : " + maps.size()) ;
    return maps;
  }  
  
      
          
}


class MapMapper implements RowMapper<map> {

  public map mapRow(ResultSet rs, int arg1) throws SQLException {
    map maps = new map();

    maps.setMAP_CODE(rs.getString("map_code"));
    maps.setMAP_DESC(rs.getString("map_desc"));

    return maps;
  }
}