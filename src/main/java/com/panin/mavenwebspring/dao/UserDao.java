/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.dao;

/**
 *
 * @author User
 */
import com.panin.mavenwebspring.model.Login;
import com.panin.mavenwebspring.model.User;
import java.util.List;

public interface UserDao {

  
  User validateUser(Login login);
  List<User> getAllUser();
}
