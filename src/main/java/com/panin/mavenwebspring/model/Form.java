/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.model;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author User
 */
public class Form {
    private String NPWP;
     
    @NotEmpty
    private String Nama;
     
    private String Alamat;
     
    private String Kota;
     
   // @NotNull
    private map map;
    
    //private kjs kjs;
 
    //Setters and Getters
 
    public void setNPWP(String NPWP){
        this.NPWP = NPWP;
    }

    public String getNPWP(){
        return this.NPWP;
    }
    
    public void setNama(String Nama){
        this.Nama = Nama;
    }
    
    public String getNama(){
        return this.Nama ;
    }

    public void setAlamat(String Alamat){
        this.Alamat= Alamat;
    }
    
    public String getAlamat(){
        return this.Alamat;
    }
    
    public void setKota(String Kota){
        this.Kota= Kota;
    }
    
    public String getKota(){
        return this.Kota;
    }
    
    public void setMap(map map){
        this.map = map;
    }
    
    public map getmap(){
        return this.map;
    }
}
