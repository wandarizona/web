/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.model;

/**
 *
 * @author Wanda user
 */
public class User {

  private String USER_ID;
  private String USERNAME;
  private String USER_FULLNAME;
  private String CORPORATE_ID;
  private String INACTIVE_DATE;
 

  public String getUSER_ID() {
  return USER_ID;
  }

  public void setUSER_ID(String USER_ID) {
  this.USER_ID = USER_ID;
  }

  public String getUSERNAME() {
  return USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
  this.USERNAME = USERNAME;
  }

  public String getUSER_FULLNAME() {
  return USER_FULLNAME;
  }

  public void setUSER_FULLNAME(String USER_FULLNAME) {
  this.USER_FULLNAME = USER_FULLNAME;
  }

  public String getCORPORATE_ID() {
  return CORPORATE_ID;
  }

  public void setCORPORATE_ID(String CORPORATE_ID) {
  this.CORPORATE_ID = CORPORATE_ID;
  }

  public String getINACTIVE_DATE() {
  return INACTIVE_DATE;
  }

  public void setINACTIVE_DATE(String INACTIVE_DATE) {
  this.INACTIVE_DATE = INACTIVE_DATE;
  }


}