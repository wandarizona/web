/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.model;

/**
 *
 * @author User
 */
public class map {
  private String MAP_CODE;
  private String MAP_DESC;  

  
  public String getMAP_CODE() {
  return MAP_CODE;
  }

  public void setMAP_CODE(String MAP_CODE) {
  this.MAP_CODE = MAP_CODE;
  }

  public String getMAP_DESC() {
  return MAP_DESC;
  }

  public void setMAP_DESC(String MAP_DESC) {
  this.MAP_DESC = MAP_DESC;
  }
  
  @Override
    public String toString() {
        return "Map [MAP_CODE=" + MAP_CODE + ", MAP_DESC=" + MAP_DESC + "]";
    }
}
