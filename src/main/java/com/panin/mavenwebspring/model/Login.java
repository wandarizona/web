/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.model;

/**
 *
 * @author User
 */
public class Login {

  private String USERNAME;
  private String CORPORATE_ID;  
  private String TOKEN;


  public String getUSERNAME() {
  return USERNAME;
  }

  public void setUSERNAME(String USERNAME) {
  this.USERNAME = USERNAME;
  }

  public String getCORPORATE_ID() {
  return CORPORATE_ID;
  }

  public void setCORPORATE_ID(String CORPORATE_ID) {
  this.CORPORATE_ID = CORPORATE_ID;
  }
  
  public String getTOKEN() {
  return TOKEN;
  }

  public void setTOKEN(String TOKEN) {
  this.TOKEN = TOKEN;
  }

}