/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.panin.mavenwebspring.dao.UserDao;
import com.panin.mavenwebspring.model.Login;
import com.panin.mavenwebspring.model.User;
import java.util.List;

public class UserServiceImpl implements UserService {

  @Autowired
  public UserDao userDao;

  public User validateUser(Login login) {
    return userDao.validateUser(login);
  }
  
  public List<User> getAllUser(){
      return userDao.getAllUser();
  }

}