/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panin.mavenwebspring.service;

import com.panin.mavenwebspring.dao.MapDao;
import com.panin.mavenwebspring.model.map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author User
 */
public class MapServiceImpl implements MapService{
    @Autowired
  public MapDao mapDao;

//  public int register(User user) {
//    return userDao.register(user);
//  }

  public map getMAP() {
    return mapDao.getMAP();
  }
  
  public List<map> getAllMAP(){
      return mapDao.getAllMAP();
  }
  
  
}
